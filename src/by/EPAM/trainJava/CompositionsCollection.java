package by.EPAM.trainJava;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/***
 * Лекция 6
 * @author Sergii_Kotov
 * издание нескольких произведений в сборнике
 * это конкретное печатное издание. 
 */
public class CompositionsCollection {
	/***
	 * вид сборника. 
	 * журнал, брошюра, книга в тв. переплете, книга в мгк переплете
	 */
	private String type;
	/***
	 * принцип объединения произведений в сборник.
	 * стихов, научпопа, рецептов, советов по осеменению скота 
	 */
	private String genre;
	private ArrayList <Composition> composes; 
	private String name;
	private String editor;
	private String publisher;
	private int year;
	private String iSBN;
	
	
	public CompositionsCollection(){
		this("","", "","","",0,"");
	}
	
	public CompositionsCollection(String type, String genre, String name,
			String editor, String publisher, int year, String iSBN) {
		this.type = type;
		this.genre = genre;
		this.composes = new ArrayList <Composition> ();
		this.name = name;
		this.editor = editor;
		this.publisher = publisher;
		this.year = year;
		this.iSBN = iSBN;
	}
	// ------------------------------------------------------------
	// --------- для поиска в сборнике
	/***
	 * поиск объекта произведения
	 * @param b ссылка на объект книги
	 * @return true если есть в сборнике (проверка по хеш, только по автору/названию)
	 */
	public boolean findBook(Composition b) {
	  if (composes!=null) {
		for (Composition h :composes) {
 			if (h.hashCode()== b.hashCode()) {
 				return true;
 			}
		}
		return false;
	  } else {
		  return false;
	  }
	}
	/***
	 * Поиск по названию и автору. 
	 * @param title
	 * @param author
	 * @return
	 */
	public boolean findBookByTitleAuthor(String title, String author) {
		 // чтобы использовать хеш создаим объект только с двумя этими полями и поищем его
 		 Composition b = new Fiction(title.toLowerCase() ,"",author.toLowerCase(),0,null,null);
		 return findBook(b);
	}
	
	/***
	 * Получить доступ к произведению сборника по автору/названию
	 * @param title
	 * @param author
	 * @return ссылка на произведение
	 */
	public Composition getBookByTitleAuthor(String title, String author) {
		if (findBookByTitleAuthor(title, author)) {
			Composition b = new Fiction(title.toLowerCase() ,"",author.toLowerCase(),0,null,null);
			for (Composition h :composes) {
		 			if (h.hashCode()== b.hashCode()) {
		 				return h;
		 			}
			}
		}
		return null;
	}
	// --------- для поиска в сборнике
	// ------------------------------------------------------------

	/***
	 * по названию и автору ищем книгу и если нашлась - записываем в нее текст.
	 * @param title название для поиска
	 * @param author автор для поиска
	 * @return удачно записалось?
	 */
	public boolean addText(String title, String author, String text) {
		Composition b = getBookByTitleAuthor(title, author);
		if (b==null) {
			return false;
		}
		b.setText(text);
		return true;
	}
	
	/***
	 * добавим произведение в сборник.
	 * проверим на уникальность
	 * @param b ссылка на объект книги
	 * @return true если добавлено
	 */
	public boolean addBook(Composition b){
		if (findBook(b)) {
			return false;
		}
		return composes.add(b);
	}	

	/***
	 * информация о сборнике в строке
	 */
	@Override
	public String toString() {
	 	  StringBuilder s = new StringBuilder(
	 					String.format("Тип сборника: %s \nЖанр: %s \nНазвание: %s \nСоставитель: %s \nИздательство: %s \nГод издания: %s \nISBN: %s \n",
	 					type , genre ,name ,editor, publisher , year,iSBN)
	 					);
	  	  
	 	 s.append(String.format("%25s%25s%25s%25s%25s%25s%25s%25s\n","Название","Авторский жанр","Автор","Год создания","Входит в цикл","Номер в цикле", "Вид произведения", "Специальная информация"));
	  	 for (Composition h :composes) {
	 			s.append(h);
	 	}
	  	  return s.toString();  
	}	
	
	/**
	 * Сортировка с помощью внешнего компаратора
	 * 
	 */
	public void sort(Comparator<? super Composition> cm) {
		Collections.sort(composes,cm);
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((composes == null) ? 0 : composes.hashCode());
		result = prime * result + ((editor == null) ? 0 : editor.hashCode());
		result = prime * result + ((genre == null) ? 0 : genre.hashCode());
		result = prime * result + ((iSBN == null) ? 0 : iSBN.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((publisher == null) ? 0 : publisher.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		result = prime * result + year;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CompositionsCollection other = (CompositionsCollection) obj;
		if (composes == null) {
			if (other.composes != null)
				return false;
		} else if (!composes.equals(other.composes))
			return false;
		if (editor == null) {
			if (other.editor != null)
				return false;
		} else if (!editor.equals(other.editor))
			return false;
		if (genre == null) {
			if (other.genre != null)
				return false;
		} else if (!genre.equals(other.genre))
			return false;
		if (iSBN == null) {
			if (other.iSBN != null)
				return false;
		} else if (!iSBN.equals(other.iSBN))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (publisher == null) {
			if (other.publisher != null)
				return false;
		} else if (!publisher.equals(other.publisher))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		if (year != other.year)
			return false;
		return true;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}
	public ArrayList<Composition> getComposes() {
		return composes;
	}
	public void setComposes(ArrayList<Composition> composes) {
		this.composes = composes;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getEditor() {
		return editor;
	}
	public void setEditor(String editor) {
		this.editor = editor;
	}
	public String getPublisher() {
		return publisher;
	}
	public void setPublisher(String publisher) {
		this.publisher = publisher;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public String getiSBN() {
		return iSBN;
	}
	public void setiSBN(String iSBN) {
		this.iSBN = iSBN;
	}
	
	

}
