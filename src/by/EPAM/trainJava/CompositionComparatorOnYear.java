package by.EPAM.trainJava;

import java.util.Comparator;

public class CompositionComparatorOnYear implements Comparator<Composition> {
	@Override
	public int compare(Composition arg0, Composition arg1) {
		int l0=arg0.getYear();
		int l1=arg1.getYear();
		return (l0>l1)?1:((l0==l1)?0:-1);
	}
}
