package by.EPAM.trainJava;
import java.util.Comparator;

/***
 * 
 * @author Sergii_Kotov
 * сравнение по наименованию
 */
public class CompositionComparatorOnName implements Comparator<Composition> {
	@Override
	public int compare(Composition arg0, Composition arg1) {
		return arg0.getName().compareTo(arg1.getName());
	}
}
