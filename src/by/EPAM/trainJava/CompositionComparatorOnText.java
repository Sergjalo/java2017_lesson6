package by.EPAM.trainJava;

import java.util.Comparator;
/***
 * 
 * @author Sergii_Kotov
 * сравнение по длине произведения
 */
public class CompositionComparatorOnText implements Comparator<Composition> {
	@Override
	public int compare(Composition arg0, Composition arg1) {
		int l0=(arg0.getText()==null)?-1:arg0.getText().length();
		int l1=(arg1.getText()==null)?-1:arg1.getText().length();
		// по убыванию
		return (l0>l1)?-11:((l0==l1)?0:1);
	}
}
