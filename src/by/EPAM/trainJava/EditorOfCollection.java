package by.EPAM.trainJava;

import java.util.Comparator;

import by.EPAM.trainJava.Fiction.fictType;
//import java.util.Scanner;
/***
 * Лекция 6
 * @author Sergii_Kotov
 * класс для работы со сборником 
 */

public class EditorOfCollection {
	//static Scanner scanner = new Scanner(System.in);
	public static void main(String[] args) {
		MainEditorOfCollections m= new MainEditorOfCollections();
		m.bc.addBook(new Poem ("Как я копал яму", "Сага", "Молодцов М.М.", 2000,"U—́ ¦ U— | U—́ ¦ U— | U—́ ¦ U—"));
		// второй раз пытаемся поместить тот же рассказ - не дает так как при добавлении проверяем на дубликаты
		// при этом происходит и поиск по автору/названию
		m.bc.addBook(new Poem ("Как я копал яму", "Сага", "Молодцов М.М.", 2000,"U—́ ¦ U— | U—́ ¦ U— | U—́ ¦ U—"));
		m.bc.addBook(new Poem ("Как я закапывал яму", "Поэма", "Молодцов М.М.", 2010,"U—́ ¦ U— | U—́ ¦ U— | U—́ ¦ U—"));
		Fiction fictB= new Fiction("Тяжелая кишка", "Гиперреализм", "Тщедушный И.И.", 1991,fictType.Fantasy,null);
		m.bc.addBook(fictB);
		m.bc.addBook(new Fiction("Переосмысление смыслов", "Биографический очерк", "Тщедушный В.И.", 2011,fictType.Science,fictB));
		// добавим текст в один из рассказов сборника. 
		// Для этого нужна цепочка методов из BookCollection: addText - getBookByTitleAuthor - findBookByTitleAuthor
		m.bc.addText("Тяжелая кишка", "Тщедушный И.И.", "Тяжела была моя кишка. Да делать нечего, какую начальство выделило, "
				+ "с такой и существовал. А когда пришла пора идти на пенсию сдал ее с радостью. Возликовал и запил надолго."
				+ "Так и помер, чего и вам желаю.");
		System.out.println(m.bc);
		System.out.println("Текст одного из произведений сборника.");
		// найдем текст произведения по автору/названию
		Composition cm=m.bc.getBookByTitleAuthor("Тяжелая кишка", "Тщедушный И.И.");
		System.out.println((cm==null)?"Текст не найден":cm.getText());
		
		System.out.println("Отсортируем по имени");
		Comparator <Composition> compName=new CompositionComparatorOnName();
		m.bc.sort(compName);
		System.out.println(m.bc);

		System.out.println("Отсортируем по количеству году создания");
		Comparator <Composition> compY=new CompositionComparatorOnYear();
		m.bc.sort(compY);
		System.out.println(m.bc);

		System.out.println("Отсортируем по количеству текста");
		Comparator <Composition> compTxt=new CompositionComparatorOnText();
		m.bc.sort(compTxt);
		System.out.println(m.bc);

	}
}

class MainEditorOfCollections {
	 CompositionsCollection bc;
	 
	 MainEditorOfCollections(){
		 bc = new CompositionsCollection("Журнал","Проза отчаявшихся ассенизаторов","Плотный запах","Проносный Ж.Ж.", 
					"Молодая авангардия",2017,"625626232");
		 randomFill(10);
	 }
	/***
	 * заполнение коллекции для тестов случайными произведениями
	 * @param n количество произведений
	 */
 	public void randomFill(int n) {
 		for (int i=0; i<n; i++) {
		 bc.addBook(new Fiction("name"+i, "type"+Math.round(Math.random()*10), 
				 "author"+Math.round(Math.random()*10),(1980+(int)Math.round(Math.random()*10)),Fiction.fictType.Fantasy,null));
		}
 	}
 	/***
 	 * внесение произведения в сборник
 	 * @param n название книги
 	 * @param t жанр автора
 	 * @param a автор
 	 * @param y год создания
 	 
 	public void addToCollection(String n, String t, String a, int y) {
 		bc.addBook(new Composition(n,t,a,y));
 	}
 	*/
 	
}
