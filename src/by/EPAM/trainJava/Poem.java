package by.EPAM.trainJava;

/***
 * 
 * @author Sergii_Kotov
 * произведение в стихотворной форме
 */
public class Poem extends Composition{
	/***
	 * схема метрики
	 */
	String rhymeStyle; // U—́ ¦ U— | U—́ ¦ U— | U—́ ¦ U—

	
	public Poem(String rhymeStyle) {
		super();
		this.rhymeStyle = rhymeStyle;
	}
	public Poem (String n, String t, String a, int y, String metr) {
		super(n, t, a, y);
		rhymeStyle=metr;
	}


	public String getRhymeStyle() {
		return rhymeStyle;
	}

	public void setRhymeStyle(String rhymeStyle) {
		this.rhymeStyle = rhymeStyle;
	}

	@Override
	public String toString() {
		return super.toString()+ String.format("%25s%-65s\n","Poem",
				"  Метрика: "+rhymeStyle);
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		//result = prime * result + ((rhymeStyle == null) ? 0 : rhymeStyle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		Poem other = (Poem) obj;
		if (rhymeStyle == null) {
			if (other.rhymeStyle != null)
				return false;
		} else if (!rhymeStyle.equals(other.rhymeStyle))
			return false;
		return true;
	}
	
	
}
